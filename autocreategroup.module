<?php

/**
 * @file
 * Allows administrators to automatically create groups
 */

/**
 * Implementation of hook_perm().
 */
function autocreategroup_perm() {
  return array('administer autocreategroup', 'administer automatic creation of groups');
}

/**
 * Implementation of hook_menu().
 */
function autocreategroup_menu() {

  // General Settings to assocaite groups with active content types
  $items['admin/settings/autocreategroup'] = array(
    'title' => 'AutoCreateGroup',
    'page callback' => 'autocreategroup_settings',
    'page arguments' => array('general'),
    'access arguments' => array('administer autocreategroup'),
  );

  // General Settings to assocaite groups with active content types
  $items['admin/settings/autocreategroup/general'] = array(
    'title' => 'General Settings',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  // Advanced Settings to customize every combination of active and group content types
  $items['admin/settings/autocreategroup/advanced'] = array(
    'title' => 'Advanced Settings',
    'page callback' => 'autocreategroup_settings',
    'page arguments' => array('advanced'),
    'access arguments' => array('administer autocreategroup'),
    'weight' => 1,
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Implementation of hook_theme().
 */
function autocreategroup_theme() {
  return array(
    'autocreategroup_title' => array(
      'arguments' => array('type' => NULL, 'nid' => NULL),
    ),
  );
}

/**
 * Callback Functions for General and Advanced Settings of Auto Creation of Groups
 * General Settings to assocaite groups with active content types
 * Advanced Settings to customize every combination of active and group content types
 */
function autocreategroup_settings($settings = 'general') {
  $content_types = node_get_types('types');
  $types = array_keys($content_types);
  if ($types !== FALSE) {
    if (is_array($types)) {
      $groups = 0;
      $omitted = 0;
      foreach ($types as $type) {
        // Calculate number of active and group content types
        $group_type = variable_get('og_content_type_usage_'. $type, '');
        switch ($group_type) {
          case '':
          case 'omitted':
            $omitted++;
            break;
          case 'group':
            $groups++;
            break;
        }
      }

      // If there are active and group content types create and render the general and advanced settings accordingly
      if ($groups > 0 && $omitted > 0) {
        switch ($settings) {
          case 'general':
            $output = drupal_get_form('autocreategroup_settings_general');
            break;
          case 'advanced':
            $output = drupal_get_form('autocreategroup_settings_advanced');
            break;
        }
      }

      // If there are no group content types display a message
      else if ($groups == 0) {
        $output = 'There are no group content types that can be automatically created. Click '. l('here', 'admin/content/types/add') .' to add a group content type. Click '. l('here', 'admin/content/types') .' to select from an existing list of content types and make it a group content type.';
      }

      //If there are no active content types, display a message
      else if ($omitted == 0) {
        $output = 'There are no content types for which you can automatically create groups. The content types that are neither group type nor can be posted in a group are the ones for which you can automatically create groups. Click '. l('here', 'admin/content/types/add') .' to add a content type. Click '. l('here', 'admin/content/types') .' to select from an existing list of content types.';
      }
    }

    // If there is only one content type, display a message
    else {
      $output = 'There are not enough content types to create groups automatically. To add more content types click '. l('here', 'admin/content/types/add') .'. Make sure you have at least one group node before trying to configure automatic creation of groups per content type.';
    }
  }

  // If there are no content types, display a message
  else {
    $output = 'Currently, there are no content types. Click '. l('here', 'admin/content/types/add') .' to add some content types. Make sure you have at least one group node before trying to configure automatic creation of groups per content type.';
  }

  return $output;
}

/**
 * Function to return the form for general settings after validating the presence of active and group content types
 */
function autocreategroup_settings_general() {
  $content_types = node_get_types('types');
  $types = array_keys($content_types);
  $active_types = array();
  $group_types = array();

  // create an array of active and group content types
  foreach ($types as $type) {
    $group_type = variable_get('og_content_type_usage_'. $type, '');
    switch ($group_type) {
      case '':
      case 'omitted':
        $active_types[] = $type;
        break;
      case 'group':
        $group_types[] = $type;
        break;
    }
  }

  $active_types = _autocreategroup_classify_content_types('active');
  $group_types = _autocreategroup_classify_content_types('group');

  // browse all the active content types for which groups can be automatically created
  foreach ($active_types as $active_type) {
    $active_name = _autocreategroup_get_name($active_type);

    // fieldset for every active content type
    $form[$active_type] = array(
      '#type' => 'fieldset',
      '#title' => t('Automatically Create Groups for every !name', array('!name' => $active_name)),
      '#weight' => 5,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#description' => t('Automatically create the following groups for every <em>!name</em>', array('!name' => $active_name)),
    );
    $options = array();
    $default_options = array();

    // browse all the content types which are treated as group nodes
    foreach ($group_types as $group_type) {
      $group_name = _autocreategroup_get_name($group_type);
      $options[$group_type] = $group_name;
    }

    // checkboxes with group names as options for each active content type
    $form[$active_type][$active_type .'_autocreategroups'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Available group types'),
      '#default_value' => variable_get($active_type .'_autocreategroups', array()),
      '#options' => $options,
    );
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#weight' => 10,
  );

  return $form;
}

/**
 * Function to return the form for advanced settings after validating the presence of active and group content types
 */
function autocreategroup_settings_advanced() {
  $content_types = node_get_types('types');
  $types = array_keys($content_types);
  $active_types = array();
  $group_types = array();

  // create an array of active and group content types
  foreach ($types as $type) {
    $group_type = variable_get('og_content_type_usage_'. $type, '');
    switch ($group_type) {
      case '':
      case 'omitted':
        $active_types[] = $type;
        break;
      case 'group':
        $group_types[] = $type;
        break;
    }
  }

  $form = array();
  sort($active_types);
  sort($group_types);

  // browse all the active content types for which groups can be automatically created
  foreach ($active_types as $active_type) {
    $active_name = _autocreategroup_get_name($active_type);
    $options = array();
    $default_options = array();
    // browse all the active groups for the current content type
    $active_groups = variable_get($active_type .'_autocreategroups', '');
    if (is_array($active_groups)) {
      foreach ($active_groups as $active_group) {
        if ($active_group !== 0) {
          $active_group_name = _autocreategroup_get_name($active_group);

          // fieldset for every active content type and group type
          $form[$active_type .'_'. $active_group] = array(
            '#type' => 'fieldset',
            '#title' => t('!group for !name', array('!group' => $active_group_name, '!name' => $active_name)),
            '#weight' => 5,
            '#collapsible' => TRUE,
            '#collapsed' => FALSE,
            '#description' => t('Customize settings for automatically creating !group for every !name', array('!group' => $active_group_name, '!name' => $active_name)),
          );

          // Customize Title of the group
          $form[$active_type .'_'. $active_group][$active_type .'_'. $active_group .'_autocreategroup_og_title'] = array(
            '#type' => 'textfield',
            '#title' => t('Title'),
            '#default_value' => variable_get($active_type .'_'. $active_group .'_autocreategroup_og_title', '[node-title] '. $active_group_name),
            '#size' => 60,
            '#maxlength' => 128,
            '#required' => TRUE,
            '#weight' => 6,
            '#description' => 'Use [node-title] as a token to be replaced by the title of the node.',
          );

          // Customize Description of the group
          $form[$active_type .'_'. $active_group][$active_type .'_'. $active_group .'_autocreategroup_og_description'] = array(
            '#type' => 'textfield',
            '#title' => t('Description'),
            '#default_value' => variable_get($active_type .'_'. $active_group .'_autocreategroup_og_description', 'This '. $active_group_name .' is dedicated to [node-title].'),
            '#size' => 70,
            '#maxlength' => 150,
            '#required' => TRUE,
            '#weight' => 7,
            '#description' => 'Use [node-title] as a token to be replaced by the title of the node.',
          );

          // Should the group appear in public listing
          $form[$active_type .'_'. $active_group][$active_type .'_'. $active_group .'_autocreategroup_og_directory'] = array(
            '#type' => 'checkbox',
            '#title' => t('List in groups directory'),
            '#default_value' => variable_get($active_type .'_'. $active_group .'_autocreategroup_og_directory', 0),
            '#weight' => 8,
            '#description' => t('Should this group appear on the '. l('list of groups page', 'og') .' (requires OG Views module)? Disabled if the group is set to <em>private group</em>.'),
          );

          $options = array(
            t('Open - membership requests are accepted immediately.'),
            t('Moderated - membership requests must be approved.'),
            t('Invite only - membership must be created by an administrator.'),
            t('Closed - membership is exclusively managed by an administrator.'),
          );

          // Customize Membership Requests of the group
          $form[$active_type .'_'. $active_group][$active_type .'_'. $active_group .'_autocreategroup_og_selective'] = array(
            '#type' => 'radios',
            '#title' => t('Membership requests'),
            '#required' => TRUE,
            '#default_value' => variable_get($active_type .'_'. $active_group .'_autocreategroup_og_selective', 0),
            '#options' => $options,
            '#weight' => 9,
            '#description' => t('How should membership requests be handled in this group? When you select <em>closed</em>, users will not be able to join <strong>or</strong> leave.'),
          );

          // Should the group be provate
          $form[$active_type .'_'. $active_group][$active_type .'_'. $active_group .'_autocreategroup_og_private'] = array(
            '#type' => 'checkbox',
            '#title' => t('private Group'),
            '#default_value' => variable_get($active_type .'_'. $active_group .'_autocreategroup_og_private', 0),
            '#weight' => 10,
            '#description' => t('Should this group be visible only to its members? Disabled if the group is set to <em>List in Directory</em> or <em>Membership requests: open</em>.'),
            '#element_validate' => array('_autocreategroup_validate_og_private'),
          );

          // Customize Membership Requests of the group
          $form[$active_type .'_'. $active_group][$active_type .'_'. $active_group .'_autocreategroup_og_register'] = array(
            '#type' => 'checkbox',
            '#title' => t('Registration form'),
            '#default_value' => variable_get($active_type .'_'. $active_group .'_autocreategroup_og_register', 0),
            '#weight' => 11,
            '#description' => t('May users join this group during registration? If checked, a corresponding checkbox will be added to the registration form.'),
          );
        }
      }
    }
    else {
      // fieldset for every active content type and group type
      $form['autocreategroup_advanced'] = array(
        '#type' => 'fieldset',
        '#title' => t('Advanced Settings'),
        '#weight' => 5,
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
        '#description' => t('Please click '. l('here', 'admin/settings/autocreategroup/general') .' and proceed to select group types that need to be automatically created for the active content types.'),
      );
    }
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#weight' => 20,
  );

  return $form;
}

// Submit handler for general settings
function autocreategroup_settings_general_submit($form, &$form_state) {
  $output = '';
  foreach ($form_state['values'] as $key => $value) {
    if (strpos($key, '_autocreategroups') !== FALSE) {
      variable_set($key, $value);
      $group_types = _autocreategroup_classify_content_types('group');
      foreach ($group_types as $group) {
        $og_variables = str_replace('autocreategroups', $group, $key);
        $exists = ($value[$group] !== 0)? 1 : 0;
        switch ($exists) {
          case 1:
            variable_set($og_variables .'_autocreategroup_og_title', '[node-title] '. $value[$group]);
            variable_set($og_variables .'_autocreategroup_og_description', 'This '. $value[$group] .' is dedicated to [node-title].');
            variable_set($og_variables .'_autocreategroup_og_directory', 0);
            variable_set($og_variables .'_autocreategroup_og_selective', '0');
            variable_set($og_variables .'_autocreategroup_og_private', 0);
            variable_set($og_variables .'_autocreategroup_og_register', 0);
            break;
          case 0:
            variable_del($og_variables .'_autocreategroup_og_title');
            variable_del($og_variables .'_autocreategroup_og_description');
            variable_del($og_variables .'_autocreategroup_og_directory');
            variable_del($og_variables .'_autocreategroup_og_selective');
            variable_del($og_variables .'_autocreategroup_og_private');
            variable_del($og_variables .'_autocreategroup_og_register');
            break;
        }
      }
    }
  }
  drupal_set_message('Your settings have been saved.'. $output);
}

// Submit handler for advanced settings
function autocreategroup_settings_advanced_submit($form, &$form_state) {
  foreach ($form_state['values'] as $key => $value) {
    if (strpos($key, '_og_') !== FALSE) {
      variable_set($key, $value);
    }
  }
  drupal_set_message('Your settings have been saved.');
}

/**
 * Function to validate private groups
 */
function _autocreategroup_validate_og_private($element, &$form_state) {
  $directory_listing = str_replace('private', 'directory', $element['#name']);
  $membership_type = str_replace('private', 'selective', $element['#name']);
  if ($element['#value'] == 1 && $form_state['values'][$directory_listing] == 1) {
    form_set_error($element['#name'], t($element['#name'] .'Listing to directory and private group cannot exist simultaneously.'));
  }
  if ($element['#value'] == 1 && $form_state['values'][$membership_type] < 1) {
    form_set_error($element['#name'], t('Open Membership and private group cannot exist simultaneously.'));
  }
}

/**
 * Implementation of hook_nodeapi().
 */
function autocreategroup_nodeapi(&$node, $op, $teaser = NULL, $page = NULL) {
  switch ($op) {
    case 'view':
      //print '<pre>'; print_r($node); print '</pre>';
      $active_types = _autocreategroup_classify_content_types('active');
      $group_types = _autocreategroup_classify_content_types('group');
      if (in_array($node->type, $active_types)) {
        $has_related_group = db_result(db_query("SELECT count(*) FROM {autocreategroup} WHERE nid = %d", $node->nid));
        if ($has_related_group) {
          $node->content['autocreategroup_reference'] = array();
          $query = db_query("SELECT group_nid FROM {autocreategroup} WHERE nid = %d", $node->nid);
          while ($result = db_fetch_array($query)) {
            $related_group = $result['group_nid'];
            $related_group_name = db_result(db_query("SELECT type FROM {node} WHERE nid = %d", $related_group));
            $node->content['autocreategroup_reference'][] = array(
              '#type' => 'item',
              '#value' => theme('autocreategroup_title', $related_group_name, $related_group),
            );
          }
        }
      }
      if (in_array($node->type, $group_types)) {
        $has_related_content = db_result(db_query("SELECT count(*) FROM {autocreategroup} WHERE group_nid = %d", $node->nid));
        if ($has_related_content) {
          $related_content = db_result(db_query("SELECT nid FROM {autocreategroup} WHERE group_nid = %d", $node->nid));
          $related_content_name = db_result(db_query("SELECT type FROM {node} WHERE nid = %d", $related_content));
          $node->content['autocreatgroup_reference'] = array(
            '#type' => 'item',
            '#value' => theme('autocreategroup_title', $related_content_name, $related_content),
          );
        }
      }
      break;
    case 'insert':
    $active_types = _autocreategroup_classify_content_types('active');
      $group_types = _autocreategroup_classify_content_types('group');
        if (in_array($node->type, $active_types)) {
          $autocreategroups = variable_get($node->type .'_autocreategroups', '');
          $do_autocreategroup = ($autocreategroups === '') ? 0 : 1;
          if ($do_autocreategroup) {
            if (is_array($autocreategroups)) {
              foreach ($autocreategroups as $group) {
                if ($group !== 0) {
                  // creating a node object
                  $group_node = new stdClass();
                  $group_node->status = 1;
                  $group_node->uid = $node->uid;
                  $group_node->type = $group;

                  // assign node title using token replacement
                  $title = variable_get($node->type .'_'. $group .'_autocreategroup_og_title', '');
                  $title = str_replace('[node-title]', $node->title, $title);
                  $group_node->title = $title;

                  // assign description using token replacement
                  $description = variable_get($node->type .'_'. $group .'_autocreategroup_og_description', '');
                  $description = str_replace('[node-title]', $node->title, $description);
                  $group_node->og_description = $description;

                  // assign membership type
                  $membership_type = variable_get($node->type .'_'. $group .'_autocreategroup_og_selective', '');
                  $group_node->og_selective = $membership_type;

                  // assign directory listing
                  $directory_listing = variable_get($node->type .'_'. $group .'_autocreategroup_og_directory', '');
                  $group_node->og_directory = $directory_listing;

                  // enable group on registration, if necessary
                  $registration = variable_get($node->type .'_'. $group .'_autocreategroup_og_register', '');
                  $group_node->og_register = $registration;

                  // assign private groups, if necessary
                  $private_group = variable_get($node->type .'_'. $group .'_autocreategroup_og_private', '');
                  $group_node->og_private = $private_group;

                  $group_node->picture = $node->picture;

                  $group_node->data = $node->data;

                  // save it and give it the rest of the attributes
                  node_save($group_node);

                  // save the relation of the group node with the parent node
                  $autocreategroup = new stdClass();
                  $autocreategroup->nid = $node->nid;
                  $autocreategroup->group_nid = $group_node->nid;
                  drupal_write_record('autocreategroup', $autocreategroup);
                }
              }
            }
          }
        }
      break;
    case 'delete':
      db_query("DELETE FROM {autocreategroup} WHERE nid = %d OR group_nid = %d", $node->nid);
      break;
  }
}

/**
 * Function to return Human name for a content type from the Machine name of a content type
 */
function _autocreategroup_get_name($type) {
  $name = db_result(db_query("SELECT name FROM {node_type} WHERE type = '%s'", $type));
  return $name;
}

/**
 * Function to return active or group content types
 */

function _autocreategroup_classify_content_types($class = 'active') {
  $content_types = node_get_types('types');
  $types = array_keys($content_types);
  $active_types = array();
  $group_types = array();

  // create an array of active and group content types
  foreach ($types as $type) {
    $group_type = variable_get('og_content_type_usage_'. $type, '');
    switch ($group_type) {
      case '':
      case 'omitted':
        $active_types[] = $type;
        break;
      case 'group':
        $group_types[] = $type;
        break;
    }
  }

  sort($active_types);
  sort($group_types);

  switch ($class) {
    case '':
    case 'active':
      return $active_types;
      break;
    case 'group':
      return $group_types;
      break;
  }
}

/**
 * Theme function to display the references on content types
 */

function theme_autocreategroup_title($type, $nid) {
  $label = db_result(db_query("SELECT name FROM {node_type} WHERE type = '%s'", $type));
  $title = db_result(db_query("SELECT title FROM {node} WHERE nid = %d", $nid));
  return '<div class="autocreategroup-label">'. $label .' : </div><div class="autocreategroup-title">'. l($title, 'node/'. $nid) .'</div>';
}
